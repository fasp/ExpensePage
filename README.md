This tool provides a cost sharing method to split expenses among multiple parties with a minimal number of transactions.
While the algorithm in principle obviously works for an arbitrary number of people, it should be noted that there probably
is no efficient solution as the problem is reducible to the Subset Sum Problem and therefore conjecturably np-complete. 