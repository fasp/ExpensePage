from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext.webapp import template
import os#, web
import Expenses as expenses
class MainPage(webapp.RequestHandler):
    
    
    def get(self):
        txt = self.request.upath_info
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('Hello, Fabians webapp World!')
    
    def post(self, *args):
        webapp.RequestHandler.post(self, *args)

class ProductHandler(webapp.RequestHandler):
    def get(self, product_id):
        self.response.write('This is the ProductHandler. '
            'The product id is %s' % product_id)

class AnyHandler(webapp.RequestHandler):
    def _set_headers(self, msg):
        self.send_response(200, msg)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        
    def get(self, id):
        name = self.request.upath_info
        if name[0]=='/':
            name=name[1:]
        ext = name.split(".")[-1] # Gather extension
 
        cType = {
            "png":"images/png",
            "jpg":"images/jpeg",
            "gif":"images/gif",
            "ico":"images/x-icon"            }
        if ext in cType:
            if name in os.listdir('img'):  # Security
                print 'hi'
#                 web.header("Content-Type", cType[name]) # Set the Header
                self.response.headers['Content-Type'] = "image/png"
                return open('img/%s'%name,"rb").read() # Notice 'rb' for reading images
            else:
                print 'hi'
                #raise web.notfound()
        else:
            path = os.path.join(os.path.dirname(__file__), 'form.html') 
            self.response.out.write(template.render(path, {}))
# #         if name.endswith('.png'):
# #             image = open('valid.png', 'r')
# #             image_file = io.BytesIO(image.read())
# #             im = Image.open(image_file)
# #             self.response.headers['Content-Type'] = "image/png"
# #             return self.response.out.write(image.image)
#         path = os.path.join(os.path.dirname(__file__), 'form.html') 
#         self.response.out.write(template.render(path, {}))   
#         #self.response.write('This is the AnyHandler. '+'The product id is %s' % id)

    def post(self, *args):
        payments = self.processCalcPostRequest()
        #self._set_headers("<html><body>hallo</body></html>")
        #self.wfile.write(payments)
        self.response.out.write(payments)
        #webapp.RequestHandler.post(self, *args)                
#application = webapp.WSGIApplication([('/', MainPage)], debug=True)

    def processCalcPostRequest(self):
        exp={}
        n = self.request.get('name1')
        v = self.request.get('exp1')
        i=1
        while n is not None:
            exp[n]=float(v)
            i+=1
            ns="name"+str(i)
            vs="exp"+str(i)
            n = self.request.get(ns)
            v = self.request.get(vs)
            n = None if not n else n 
        payments = expenses.calc(exp)
        return payments
        #self._set_headers("<html><body>hallo</body></html>")
        #self.wfile.write("<html><body><h1>POST!</h1></body></html>")
        

application = webapp.WSGIApplication([
                                      #('/', MainPage),
                                      #s(r'/products/(\d+)', ProductHandler),
                                      (r'(\S+)', AnyHandler),
                                      #(r'/(\S+)', AnyHandler),
                                      ], debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
