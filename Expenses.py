# exp=sorted({'Fabian':175,
#   'Joern':247,
#   'Lennart':506
#   #, 'anotherOne':312
#   #, 'evenMore':700
#   }.iteritems(), key=lambda (k,v): (v,k))
 
def fOut(f):
    return "{:4.2f}".format(f)

def calc(exp):
    exp=sorted(exp.iteritems(), key=lambda (k,v): (v,k))
 
    gets=[None]*len(exp)
    div=1.0/len(exp)
    prevDebt=0
    payments =''
    for i in range(len(exp)-1):
        payments+= exp[i][0]
        debt=0
        for o in range(i+1, len(exp)):
            if i!=o:
                pay=div*(exp[o][1]-exp[i][1])
                gets[o]=pay
                payments+= '\t owes '+exp[o][0]+' '+fOut(div*exp[o][1])+' - '+fOut(div*exp[i][1])+' = '+fOut(pay)+'<br>'
                debt+=pay
        keep=''
        if i!=0:
            prevDebt=prevDebt+debt-gets[i]
            keep=', keeps '+fOut(gets[i])
        else:
            prevDebt=debt
        payments+= '=> pays '+fOut(prevDebt)+' in total to '+exp[i+1][0]+keep+'<br>'+60*'*'+'<br>'
    return payments